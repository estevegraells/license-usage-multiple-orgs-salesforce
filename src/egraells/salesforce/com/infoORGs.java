/**
* Obtener información de Licencias de 1 o varias ORGs de Salesforce.
* 	
* Descripción: A partir de un fichero de conexiones de entrada se genera un fichero de salida XML con la info de las licencias usadas en todas las orgs.
* El formateado de salida se lleva a cabo con una simple transformación en una hoja XSL y usando el CSS de Twitter Bootstrap. 
* 
*  Para obtener la información del uso de las licencias de las ORG se accede mediante el objeto UserLicense https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_userlicense.htm
*  Varios de los campos usados en la query requieren la apertura de un ticket a Salesforce para que activen la capacidad de consultarlos:
*  This field is available through the API Access to User Licenses pilot program. For information on enabling this pilot program for your organization, contact Salesforce.
*  
*  El fichero orgs.xml contiene la lista de las ORGs que queremos consultar con el siguiente formato:
*  <orgs>
*  	<conexion> 
*  		<descripcionorg>Texto descriptivo para reconocer a la ORG</descripcion>
		<username>username</username>
		<password>password+accesstoken</password>
*   </conexion>
*  </orgs>
*  
* Descarga de las librerías de Salesforce: http://mvnrepository.com/artifact/com.force.api
*
* @author  Esteve Graells 
* @version 1.0
* @since   01-08-2016 
*/

package egraells.salesforce.com;

import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

import com.sforce.soap.partner.sobject.*;
import com.sforce.soap.partner.*;

import java.util.Arrays;
import java.util.List;
import java.io.File;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class infoORGs {

	static final String AUTHENDPOINT = "https://login.salesforce.com/services/Soap/u/37.0";

	static final String PATH_FILE_CONEXIONES_ORGS = "./config/orgs.xml";
	
	static final String PATH_FILE_LICENCIAS_ORGS = "./config/lics.xml";

	static final List<String> licenciasObservadas = Arrays.asList();
	
	static final DocumentBuilderFactory dbFactory = null;
	static final DocumentBuilder docBuilder = null;
	
	static final File fichConexiones = null;
		
	static final Document docLicsOutput = null;
	static final Document docCnxsInput = null;

	static PartnerConnection connection;

	public static void main(String[] args) {

		infoORGs infoORGs = new infoORGs();

		try {
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
						
			//Configuracion fichero XML entrada
			File fConexiones = new File(PATH_FILE_CONEXIONES_ORGS);
			Document xmlEntrada = docBuilder.parse(fConexiones);
			xmlEntrada.getDocumentElement().normalize();// optional, but recommended read this -http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work

			//Configuracion fichero XML salida de resultados
			Document xmlSalida = docBuilder.newDocument();
			
			//creacion nodo raiz
			Element rootElement = xmlSalida.createElement("licenciasorgs");
			xmlSalida.appendChild(rootElement);	
			
			//Lectura del fichero de conexiones
			NodeList nList = xmlEntrada.getElementsByTagName("conexion");

			// Para cada conexion obtenemos info del uso de sus licencias
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					String descripcionOrg = eElement.getElementsByTagName("descripcionorg").item(0).getTextContent();
					String username = eElement.getElementsByTagName("username").item(0).getTextContent();
					String password = eElement.getElementsByTagName("password").item(0).getTextContent();

					//Establecer conexion con la ORG
					try {
						ConnectorConfig config = new ConnectorConfig();
						config.setUsername(username);
						config.setPassword(password);
						config.setAuthEndpoint(AUTHENDPOINT);
						// config.setTraceMessage(true);
						connection = Connector.newConnection(config);
					} catch (Exception e) {
						e.printStackTrace();
					}

					//Obtener info basica de la ORG
					QueryResult qIDs = connection.query(" SELECT Id, Name FROM Organization ");
					SObject so = (SObject) qIDs.getRecords()[0];
					
					Element org = xmlSalida.createElement("org");
					rootElement.appendChild(org);
					
					Node stylesheet = xmlSalida.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"lics.xsl\"");
					xmlSalida.insertBefore(stylesheet, rootElement);
					
					Element desc = xmlSalida.createElement("desc");
					desc.appendChild(xmlSalida.createTextNode(descripcionOrg));
					org.appendChild(desc);
					
					
					Element nombre = xmlSalida.createElement("nombre");
					nombre.appendChild(xmlSalida.createTextNode(so.getField("Name").toString()));
					org.appendChild(nombre);
					
					Element id = xmlSalida.createElement("id");
					id.appendChild(xmlSalida.createTextNode(so.getField("Id").toString()));
					org.appendChild(id);
					
					// infoORGs.getInfoBasicaLicencias();
					
					try {
						QueryResult qLicenciasUsadas = connection.query(" SELECT Name, Status, TotalLicenses, UsedLicenses FROM UserLicense ");

						if (qLicenciasUsadas.getSize() > 0) {
							
							System.out.println("\nUSO DE LICENCIAS \n");

							for (int i = 0; i < qLicenciasUsadas.getRecords().length; i++) {
								
								SObject soLicencias = (SObject) qLicenciasUsadas.getRecords()[i];

								String licenciaNombre = soLicencias.getField("Name").toString();
								String licenciaEstado = soLicencias.getField("Status").toString();
								
								String licTotales = soLicencias.getField("TotalLicenses").toString();
								String licUsadas = soLicencias.getField("UsedLicenses").toString();
								Integer licDisponibles = Integer.parseInt(licTotales) - Integer.parseInt(licUsadas);
								
								
								//Añadimos la info de las licencias de la ORG
								Element usoLicencias = xmlSalida.createElement("licencia");
								org.appendChild(usoLicencias);
								
								Element elNombre = xmlSalida.createElement("nombre");
								elNombre.appendChild(xmlSalida.createTextNode(licenciaNombre));
								usoLicencias.appendChild(elNombre);
								
								Element elStatus = xmlSalida.createElement("estado");
								elStatus.appendChild(xmlSalida.createTextNode(licenciaEstado));
								usoLicencias.appendChild(elStatus);
								
								Element elTotalLicencias = xmlSalida.createElement("totales");
								elTotalLicencias.appendChild(xmlSalida.createTextNode(licTotales));
								usoLicencias.appendChild(elTotalLicencias);
								
								Element elUsadasLicencias = xmlSalida.createElement("usadas");
								elUsadasLicencias.appendChild(xmlSalida.createTextNode(licUsadas));
								usoLicencias.appendChild(elUsadasLicencias);
								
								Element elDisponibleLicencias = xmlSalida.createElement("disponibles");
								elDisponibleLicencias.appendChild(xmlSalida.createTextNode(licDisponibles.toString()));
								usoLicencias.appendChild(elDisponibleLicencias);
								
							}
							
							// write the content into xml file
							TransformerFactory transformerFactory = TransformerFactory.newInstance();
							Transformer transformer = transformerFactory.newTransformer();
							DOMSource source = new DOMSource(xmlSalida);
							StreamResult result = new StreamResult(new File(PATH_FILE_LICENCIAS_ORGS));

							// Output to console for testing
							StreamResult resultOut = new StreamResult(System.out);
							
							transformer.transform(source, result);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					System.out.println("FIN");

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	// Si no se tiene acceso al objeto UserLicense solo pueden consultarse info
	// básica de licencias
	private void getInfoBasicaLicencias() {
		try {

			QueryResult qLicenciasUsadas = connection.query(
					" SELECT COUNT(ID), Profile.UserLicense.Name, Profile.UserType FROM User WHERE IsActive = true GROUP BY Profile.UserLicense.Name, Profile.UserType ");

			// QueryResult qLicenciasTotales = connection.query(
			// " SELECT TotalLicenses, Name FROM UserLicense GROUP BY
			// UserLicense ");

			if (qLicenciasUsadas.getSize() > 0) {

				for (int i = 0; i < qLicenciasUsadas.getRecords().length; i++) {
					SObject so = (SObject) qLicenciasUsadas.getRecords()[i];

					if (!licenciasObservadas.isEmpty()) {
						if (licenciasObservadas.contains(so.getField("Name"))) {
							System.out.println("Tipo Licencia: " + so.getField("Name") + " -- # Licencias Usadas: "
									+ so.getField("expr0"));
						}
					} else {
						System.out.println("Tipo Licencia: " + so.getField("Name") + " -- # Licencias Usadas: "
								+ so.getField("expr0"));
					}
				}

			} else {
				System.out.println("Esta ORG no tiene usuarios activos");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
