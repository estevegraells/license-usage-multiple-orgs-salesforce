<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">



<html>
	<head>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" />
	</head>
	
	<body>
		<div class="container-fluid">
				<h2 class="page-header">Uso de Licencias Salesforce</h2>
				<xsl:for-each select="licenciasorgs/org">
					
					<h5><b>Descripción: </b> <xsl:value-of select="desc"/> - Nombre Técnico: <xsl:value-of select="nombre"/> - Id: <xsl:value-of select="id"/></h5>
					<table class="table table-condensed table-bordered">
						<th>Nombre</th>
						<th>Estado</th>
						<th>Totales</th>
						<th>Usadas</th>
						<th>Disponibles</th>
						<xsl:for-each select="licencia">
							<tr>
								<td> <xsl:value-of select="nombre"/> </td>
								<td> <xsl:value-of select="estado"/> </td>
								<td> <xsl:value-of select="totales"/> </td>
								<td> <xsl:value-of select="usadas"/> </td>
								<td> <xsl:value-of select="disponibles"/> </td>
							</tr>
						</xsl:for-each>						
					</table>							
				</xsl:for-each>
		</div>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>