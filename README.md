# Obtener información del consumo de Licencias en nuestras ORGs #

## Descripción ##
A partir de un fichero de conexiones de entrada se genera un fichero de salida XML con la info de las licencias usadas en todas las orgs.

## Aspectos Técnicos ##

### Conexión a las ORGs ###
Se accede mediante la API y las librerias para acceso Java pueden descargarse aquí: http://mvnrepository.com/artifact/com.force.api

### Como obtener la información ###
Para obtener la información del uso de las licencias de las ORG se accede mediante el objeto UserLicense https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_userlicense.htm
Varios de los campos usados en la query requieren la apertura de un ticket a Salesforce para que activen la capacidad de consultarlos:
This field is available through the API Access to User Licenses pilot program. For information on enabling this pilot program for your organization, contact Salesforce.

### Fichero de entrada ###
El fichero orgs.xml contiene la lista de las ORGs que queremos consultar con el siguiente formato:
```
	<orgs>
		<conexion> 
			<descripcionorg>Texto descriptivo para reconocer a la ORG</descripcion>
			<username>username</username>
			<password>password+accesstoken</password>
		</conexion>
	</orgs>
```
El password es la concatenación de password y accestoken sin ningún carácter intermedio.
### Fichero de salida ###
El resultado del proceso genera un fichero XML, que tiene relacionado una hoja de transformación XSL para mostrarlo adecuadamente.
Adicionalmente se una una hoja de estilos CSS de Twitter Bootstrap. 
Por tanto debe ejecutarse el proceso y abrir el fichero lics.xml en tu navegador